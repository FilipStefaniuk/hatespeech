# Multilingual Hate Speech Detection

## Data

### Languages Data
A collection of the hate speech dataset form various competitions in different languages. They are split into train, dev and test parts.

<table>
    <tr>
        <th>Language</th>
        <th>Train</th>
        <th>Dev</th>
        <th>Test</th>
        <th>Total</th>
        <th>Data link</th>
        <th>Reference</th>
    </tr>
    <tr>
        <td>Arabic</td>
        <td>4676</td>
        <td>585</td>
        <td>585</td>
        <td>5846</td>
        <td><a href="https://github.com/Hala-Mulki/L-HSAB-First-Arabic-Levantine-HateSpeech-Dataset">[link]</a></td>
        <td><a href="https://drive.google.com/file/d/1527OBXQ_IzxutYGClD1xN8AWhnmghFCs/view">[link]</a></td>
    </tr>
    <tr>
        <td>English</td>
        <td>9000</td>
        <td>1000</td>
        <td>2971</td>
        <td>12971</td>
        <td><a href="https://competitions.codalab.org/competitions/19935">[link]</a></td>
        <td><a href="https://www.aclweb.org/anthology/S19-2007/">[link]</a></td>
    </tr>
    <tr>
        <td>Spanish</td>
        <td>4500</td>
        <td>500</td>
        <td>1600</td>
        <td>6600</td>
        <td><a href="https://competitions.codalab.org/competitions/19935">[link]</a></td>
        <td><a href="https://www.aclweb.org/anthology/S19-2007/">[link]</a></td>
    </tr>
    <tr>
        <td>Italian</td>
        <td>2499</td>
        <td>499</td>
        <td>999</td>
        <td>3997</td>
        <td><a href="http://www.di.unito.it/~tutreeb/haspeede-evalita18/index.html#">[link]</a></td>
        <td><a href="http://ceur-ws.org/Vol-2263/paper010.pdf">[link]</a></td>
    </tr>
    <tr>
        <td>Polish</td>
        <td>9040</td>
        <td>999</td>
        <td>999</td>
        <td>11038</td>
        <td><a href="http://poleval.pl/tasks/task6">[link]</a></td>
        <td><a href="http://poleval.pl/files/poleval2019.pdf">[link]</a></td>
    </tr>
    <tr>
        <td>Portuguese</td>
        <td>4536</td>
        <td>567</td>
        <td>567</td>
        <td>5670</td>
        <td><a href="https://github.com/paulafortuna/Portuguese-Hate-Speech-Dataset">[link]</a></td>
        <td><a href="https://drive.google.com/file/d/1g1WlNjmTTUeWyuJFYogjnmfZVB70b7_r/view">[link]</a></td>
    </tr>
</table>

### English data
Open hate speech datasets in english. They are not split and most of them are unbalanced.
<table>
    <tr>
        <th>Dataset</th>
        <th>Hate</th>
        <th>Non-hate</th>
        <th>Total</th>
        <th>Data link</th>
        <th>Reference</th>
    </tr>
    <tr>
        <td>Davidson et. al. (dt)</td>
        <td>1430 (5.8%)</td>
        <td>23353 (94.2%)</td>
        <td>24783</td>
        <td><a href="https://github.com/t-davidson/hate-speech-and-offensive-language">[link]</a></td>
        <td><a href="https://aaai.org/ocs/index.php/ICWSM/ICWSM17/paper/view/15665">[link]</a></td>
    </tr>
    <tr>
        <td>Waseem & Hovy (wz)</td>
        <td>3490 (26%)</td>
        <td>9775 (74%)</td>
        <td>13265</td>
        <td><a href="https://github.com/zeerakw/hatespeech">[link]</a></td>
        <td><a href="https://www.aclweb.org/anthology/N16-2013/">[link]</a></td>
    </tr>
    <tr>
        <td>Founta et. al. (lsc)</td>
        <td>4965 (5%)</td>
        <td>95030 (95%)</td>
        <td>99995</td>
        <td><a href="https://github.com/ENCASEH2020/hatespeech-twitter">[link]</a></td>
        <td><a href="https://arxiv.org/pdf/1802.00393.pdf">[link]</a></td>
    </tr>
    <tr>
        <td>Golbeck et. al. (oh)</td>
        <td>5285 (26%)</td>
        <td>15075 (74%)</td>
        <td>20360</td>
        <td>-</td>
        <td><a href="http://www.cs.umd.edu/~golbeck/papers/trolling.pdf">[link]</a></td>
    </tr>
</table>

I have merged and balanced the dataset by undersampling non-hate examples creating the combined dataset for pretraining. It is splitted into train and dev parts. The data were split in a stratified manner to sample from all the datasets.
<table>
    <tr>
        <th>Part</th>
        <th>Hate</th>
        <th>Non-hate</th>
        <th>Total</th>
    </tr>
    <tr>
        <td>train</td>
        <td>13652 (50%)</td>
        <td>13654 (50%)</td>
        <td>27306</td>
    </tr>
    <tr>
        <td>dev</td>
        <td>1518 (50%)</td>
        <td>1516 (50%)</td>
        <td>3034</td>
    </tr>
</table>

## Embeddings
In this project I am using multilingual word embeddings and emoji embeddings. Files can be downloaded using the `bin/download_vectors.sh` script. In addition I created embeddings for special normalization tokens, which are included with the project and were created using `build_special_emb` script with `wiki.multi.en.vec`.

In the table below I show out of vocabulary tokens for each language (on training parts).

<table>
<tr>
    <th>Language</th>
    <th>OOV</th>
    <th>OOV * Counts</th>
</tr>
<tr>
    <td>Arabic</td>
    <td>8673 / 17971 (48.26%)</td>
    <td>12900 / 56674 (22.76%)</td>
</tr>
<tr>
    <td>English</td>
    <td>2941 / 15995 (18.39 %)</td>
    <td>4288 / 205150 (2.09 %)</td>
</tr>
<tr>
    <td>Spanish</td>
    <td>3251 / 14265 (22.79 %)</td>
    <td>5183 / 96200 (5.39 %)</td>
</tr>
<tr>
    <td>Italian</td>
    <td>1094 / 7839 (13.96 %)</td>
    <td>1527 / 45055 (3.39 %)</td>
</tr>
<tr>
    <td>Polish</td>
    <td>6976 / 21170 (32.95 %)</td>
    <td>9796 / 110868 (8.84 %)</td>
</tr>
<tr>
    <td>Portuguese</td>
    <td>1642 / 11104 (14.79 %)</td>
    <td>2210 / 71864 (3.08 %)</td>
</tr>
</table>

Similar table fro the emojis:
<table>
<tr>
    <th>Language</th>
    <th>OOV</th>
    <th>OOV * Counts</th>
</tr>
<tr>
    <td>Arabic</td>
    <td>7 / 17 (41.18 %)</td>
    <td>9 / 73 (53.42 %)</td>
</tr>
<tr>
    <td>English</td>
    <td>28 / 195 (14.36 %)</td>
    <td>272 / 1379 (19.72 %)</td>
</tr>
<tr>
    <td>Spanish</td>
    <td>35 / 217 (16.13 %)</td>
    <td>221 / 1324 (16.69 %)</td>
</tr>
<tr>
    <td>Italian</td>
    <td>10 / 42 (23.81 %)</td>
    <td>17 / 89 (19.10 %)</td>
</tr>
<tr>
    <td>Polish</td>
    <td>18 / 142 (12.68 %)</td>
    <td>102 / 2546 (4.01 %)</td>
</tr>
<tr>
    <td>Portuguese</td>
    <td>-</td>
    <td>-</td>
</tr>
</table>


## Training and Evaluation
Models can be trained and evaluated using `allennlp` API.

### Training
To train model run:
```
allennlp train CONFIG -s SERIALIZATION_DIR --include-package hatespeech
``` 
where:
- `CONFIG` is a `.jsonnet` file with model description and hyperparameters
- `SERIALIZATION_DIR` is path to a directory where the model will be saved

### Fine tuning
To fine tune model for specific dataset:
```
allennlp fine-tune -m MODEL_ARCHIVE -c CONFIG_FILE -s SERIALIZATION_DIR --include-package hatespeech
```
where
- `MODEL_ARCHIVE` is directory where model wa saved (`SERIALIZATION_DIR` from `train` command)
- `CONFIG` is configuration file for fine tuning, model section is ignored
- `SERIALIZATION_DIR` is directory where new model will be saved

### Evaluation
To evaluate model:
```
allennlp evaluate SERIALIZATION_DIR EVALUATION_DATA --include-package hatespeech
```
where
- `SERIALIZATION_DIR` is path to a directory where model was saved
- `EVALUATION_DATA` is path to evaluation dataset

### Results

#### Different modules

<table>
<tr>
    <th>Language</th>
    <th>LSTM<br>+CNN+emoji</th>
    <th>CNN+emoji</th>
    <th>LSTM+CNN</th>
    <th>CNN</th>
    <th>Self Attention</br> + CNN</th>
    <th>Self Attention</br> + CNN</br> + emoji</th>
</tr>
<tr>
    <td>Arabic</td>
    <td>-</td>
    <td>-</td>
    <td>0.628</td>
    <td>0.678</td>
    <td><b>0.699</b></td>
    <td>-</td>
</tr>
<tr>
    <td>English</td>
    <td>0.394</td>
    <td>0.359</td>
    <td>0.360</td>
    <td>0.364</td>
    <td>0.354</td>
    <td><b>0.402</b></td>
</tr>
    <td>English*</td>
    <td>0.692</td>
    <td>0.759</td>
    <td>0.755</td>
    <td>0.731</td>
    <td><b>0.764</b></td>
    <td>0.755</td>
<tr>
</tr>
<tr>
    <td>Spanish</td>
    <td>0.685</td>
    <td><b>0.736</b></td>
    <td>0.707</td>
    <td>0.731</td>
    <td>0.722</td>
    <td>0.724</td>
</tr>
<tr>
    <td>Italian</td>
    <td>-</td>
    <td>-</td>
    <td>0.748</td>
    <td>0.742</td>
    <td><b>0.754</b></td>
    <td>-</td>
</tr>
<tr>
    <td>Polish</td>
    <td>0.638</td>
    <td><b>0.731</b></td>
    <td>0.641</td>
    <td>0.727</td>
    <td>0.600</td>
    <td>0.589</td>
</tr>
<tr>
    <td>Portuguese</td>
    <td>-</td>
    <td>-</td>
    <td>0.708</td>
    <td><b>0.732</b></td>
    <td>0.705</td>
    <td>-</td>
</tr>
</table>

--------------
#### Fine tuning

<table>
<tr>
    <th>Language</th>
    <th>Random</th>
    <th>Zero-Shot</th>
    <th>Trained</th>
    <th>Fine tuned</th>
</tr>
<tr>
    <td>Arabic</td>
    <td>0.478</td>
    <td>0.553</td>
    <td>0.699</td>
    <td><b>0.745</b></td>
</tr>
<tr>
    <td>English</td>
    <td>0.505</td>
    <td>0.573</td>
    <td>0.394</td>
    <td><b>0.580</b></td>
</tr>
<tr>
    <td>Spanish</td>
    <td>0.368</td>
    <td>0.541</td>
    <td>0.736</td>
    <td><b>0.739</b></td>
</tr>
<tr>
    <td>Italian</td>
    <td>0.407</td>
    <td>0.669</td>
    <td>0.754</td>
    <td><b>0.764</b></td>
</tr>
<tr>
    <td>Polish</td>
    <td>0.496</td>
    <td>0.564</td>
    <td><b>0.731</b></td>
    <td>0.672</td>
</tr>
<tr>
    <td>Portuguese</td>
    <td>0.405</td>
    <td>0.605</td>
    <td><b>0.732</b></td>
    <td>0.722</td>
</tr>
</table>

--------------
#### Comparison with SoTa 

<table>
<tr>
    <th>Language</th>
    <th>My model</th>
    <th>Baseline</th>
    <th>SoTa</th>
</tr>
<tr>
    <td>Arabic</td>
    <td>0.745</td>
    <td>-</td>
    <td>-</td>
</tr>
<tr>
    <td>English</td>
    <td>0.580</td>
    <td>0.451</td>
    <td>0.650</td>
</tr>
<tr>
    <td>Spanish</td>
    <td>0.739</td>
    <td>0.701</td>
    <td>0.730</td>
</tr>
<tr>
    <td>Italian</td>
    <td>0.764</td>
    <td>0.403</td>
    <td>0.799</td>
</tr>
<tr>
    <td>Polish</td>
    <td>0.731</td>
    <td>0.236</td>
    <td>0.585</td>
</tr>
<tr>
    <td>Portuguese</td>
    <td>0.732</td>
    <td>-</td>
    <td>-</td>
</tr>
</table>
