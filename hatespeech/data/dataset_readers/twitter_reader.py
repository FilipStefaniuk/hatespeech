import json
import sys
from typing import Dict, Iterator, List, Union, Optional

from overrides import overrides
from allennlp.data.instance import Instance
from allennlp.data.dataset_readers import DatasetReader
from allennlp.data.tokenizers import Token, Tokenizer, WordTokenizer
from allennlp.data.fields import LabelField, TextField, MetadataField
from allennlp.data.token_indexers import TokenIndexer, SingleIdTokenIndexer


@DatasetReader.register("twitter")
class TwitterReader(DatasetReader):
    def __init__(self,
                 token_indexers: Optional[Dict[str, TokenIndexer]] = None,
                 emoji_indexers: Optional[Dict[str, TokenIndexer]] = None,
                 min_num_tokens: int = None):
        super().__init__(lazy=False)
        self._token_indexers = token_indexers or {'tokens': SingleIdTokenIndexer()}
        self._emoji_indexers = emoji_indexers or {'emojis': SingleIdTokenIndexer(namespace='emojis')}
        self.min_num_tokens = min_num_tokens

    @overrides
    def text_to_instance(self, tweet: str, tokens: List[str], emojis: List[str], label: int = None) -> Instance:

        fields = {
            'tweet': MetadataField(tweet),
            'tokens': TextField([Token(token) for token in tokens], self._token_indexers),
            'emojis': TextField([Token(emoji) for emoji in emojis], self._emoji_indexers)
        }

        if label is not None:
            fields['label'] = LabelField(str(label))

        return Instance(fields)

    @overrides
    def _read(self, file_path: str) -> Union[List[Instance], Iterator[Instance]]:
        with open(file_path) as data_file:
            data = list(map(json.loads, data_file))
            num_instances = len(data)
            if self.min_num_tokens is not None:
                # TODO Add warning how many skipped
                data = list(filter(lambda x: len(x['tokens']) >= self.min_num_tokens, data))
            
            if num_instances != len(data):
                print(f"Warning: {file_path} - skipped {num_instances - len(data)} instances.", file=sys.stderr)

            data = [self.text_to_instance(instance['tweet'], instance['tokens'],
                                          instance['emojis'], instance.get('label', None)) for instance in data]
            return data
