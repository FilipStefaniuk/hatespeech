import json
import argparse


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('datasets', nargs='+')
    parser.add_argument('--print_total', action='store_false', default=True)

    return parser.parse_args()


def print_statistics(rows):
    print(f"{'DATASET':40}{'HATEFUL':^14}{'NON HATEFUL':^18}{'TOTAL':10}")
    print('-' * (40 + 14 + 18 + 10))
    for name, hate, non_hate, total in rows:
        print(f"{name:40}{hate:>6}{hate/total:>8.2}{non_hate:>10}{non_hate/total:>8.2}{total:10}")


def main():
    args = get_args()

    rows = []
    total_sum, hate_sum = 0, 0
    for dataset in args.datasets:
        total, hate = 0, 0
        with open(dataset) as dataset_file:
            for line in dataset_file:
                total += 1
                instance = json.loads(line)
                if instance['label']:
                    hate += 1
            total_sum += total
            hate_sum += hate
            rows.append((dataset, hate, total - hate, total))
    rows.append(('total', hate_sum, total_sum - hate_sum, total_sum))

    print_statistics(rows)


if __name__ == '__main__':
    main()