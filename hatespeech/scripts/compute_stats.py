import argparse
import json
import itertools
import importlib
from collections import Counter
from typing import List, Dict, Any
from operator import itemgetter

import numpy as np

SPECIAL_TOKENS = [
    "<user>",
    "<number>",
    "<url>",
    "<money>",
    "<percent>",
    "<date>",
    "<time>",
    "s",
    "-",
    "\\"
]

def get_args():
    """Parse comman line arguments"""
    parser = argparse.ArgumentParser()

    parser.add_argument("dataset", type=argparse.FileType())
    parser.add_argument("--lang", default="en")
    parser.add_argument("--key", default="tokens")
    parser.add_argument("--label", default=None, choices=[None, "1", "0"])
    parser.add_argument("--most-common", type=int, default=10)

    return parser.parse_args()

def clean_counter(counter: Counter, lang="en"):
    tokens_to_remove = SPECIAL_TOKENS
    stop_words = importlib.import_module(f"spacy.lang.{lang}.stop_words")
    tokens_to_remove += stop_words.STOP_WORDS

    for token in tokens_to_remove:
        if token in counter:
            del counter[token]


def compute_statistics(data: List, key="tokens", lang="eng", most_common=10, label=None) -> Dict[str, Any]:
    """Compute surface level statistics for a dataset."""
    if label is not None:
        data = list(filter(lambda x: x['label'] == int(label), data))
    data = list(map(itemgetter(key), data))
    counter = Counter(itertools.chain.from_iterable(data))
    avg_len = np.mean(list(map(len, data)))
    uniq = len(counter.keys())
    clean_counter(counter, lang=lang)

    return {
        "unique": uniq,
        "avg_len": avg_len,
        "most_common": counter.most_common(most_common)
    }


def print_stats(stats):
    print("*"*40)
    print(f"{'STATISTICS':^40}")
    print("*"*40)
    print()
    print("-"*40)
    print(f"{'MOST COMMON WORDS':^40}")
    print("-"*40)
    for word, count in stats['most_common']:
        print(f"{word:<30}{count:>10}")
    print("-"*40)
    print(f"NUMBER OF UNIQUE TOKENS: {stats['unique']}")
    print(f"AVERAGE LENGTH: {stats['avg_len']:.2f}")

def main():
    args = get_args()
    data = list(map(json.loads, args.dataset))
    stats = compute_statistics(data, key=args.key, lang=args.lang, most_common=args.most_common, label=args.label)
    print_stats(stats)