import os
import json
import argparse
import itertools
import random

import pandas as pd
from sklearn.utils import resample
from sklearn.model_selection import train_test_split


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('datasets', nargs='+', type=argparse.FileType(),
                        help="List of the datasets to merge.")

    parser.add_argument('--resample', action='store_true', default=False,
                        help="Whether to resample the data.")

    parser.add_argument('--random_state', type=int, default=123,
                        help="Random state for resamplling.")
    
    parser.add_argument('--test', type=float, default=0.0,
                        help="Fraction of the data for test set.")

    parser.add_argument('--validation', type=float, default=0.0,
                        help="Fraction of the data for validation set.")

    parser.add_argument('--output_dir', default='./',
                        help='Directory where to save created dataset.')
    return parser.parse_args()


def main():
    args = get_args()
    names = [os.path.splitext(os.path.basename(f.name))[0] for f in args.datasets]
    data = [pd.read_json(d, lines=True) for d in args.datasets]
    data = [d.assign(dataset=name) for name, d in zip(names, data)]
    data = pd.concat(data)

    if args.resample:
        non_hateful = data[data["label"] == 0]
        hateful = data[data["label"] == 1]

        majority, minority = (non_hateful, hateful) if len(non_hateful) > len(hateful) else (hateful, non_hateful)
        majority = resample(majority, random_state=args.random_state, n_samples=len(minority))

        data = pd.concat([minority, majority])

    if args.test + args.validation == 0.0:
        train = data.sample(frac=1).reset_index(drop=True)
        train.to_json(os.path.join(args.output_dir, "train.jsonl"), orient="records", lines=True)

    else:
        data['label_dataset'] = data['label'].astype(str) + '_' + data['dataset'].astype(str)
        train, other = train_test_split(data, test_size=(args.test + args.validation),
                                        random_state=args.random_state, stratify=data[['label_dataset']])


        if args.test != 0.0 and args.validation != 0.0:
            frac = args.test / (args.test + args.validation)
            valid, test = train_test_split(other, test_size=frac, random_state=args.random_state, stratify=other[['label_dataset']])

            valid = valid.drop(columns=['label_dataset'])
            valid.to_json(os.path.join(args.output_dir, "dev.jsonl"), orient="records", lines=True)
        
            test = test.drop(columns=['label_dataset'])
            test.to_json(os.path.join(args.output_dir, "test.jsonl"), orient="records", lines=True)

        else:
            other = other.drop(columns=['label_dataset'])
            name = "dev.jsonl" if args.test == 0.0 else "test.jsonl"
            other.to_json(os.path.join(args.output_dir, name), orient="records", lines=True)

        train = train.drop(columns=['label_dataset'])
        train.to_json(os.path.join(args.output_dir, "train.jsonl"), orient="records", lines=True)


if __name__ == '__main__':
    main()
