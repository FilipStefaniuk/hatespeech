import sys
import argparse
from typing import List, Iterable, Dict


SPECIAL_TOKENS = [
    "<user>",
    "<number>",
    "<url>",
    "<money>",
    "<percent>",
    "<date>",
    "<time>",
]


def get_args():
    """Parse command line arguments"""
    parser = argparse.ArgumentParser()
    
    parser.add_argument("emb_file", type=argparse.FileType(),
        help="Emb file that will be used to embed special tokens.")

    parser.add_argument("output_file", type=argparse.FileType("w"))

    return parser.parse_args()

def embed_sepecial_tokens(vectors: Iterable, tokens: List[str]) -> Dict[str, List[str]]:
    """Embed the special tokens using words from emb file."""

    embedded_specials = {}
    tokens = set(token.strip("<>") for token in tokens)

    # skip the header
    next(vectors)
    for vector in vectors:
        vector = vector.split()
        if vector[0] in tokens:
            embedded_specials[vector[0]] = vector[1:]

    return {f"<{k}>": v for k, v in embedded_specials.items()}


def main():
    args = get_args()
    special_emb = embed_sepecial_tokens(args.emb_file, SPECIAL_TOKENS)

    diff = set(SPECIAL_TOKENS).difference(special_emb.keys())
    
    if diff:
        print(f"Warning: Embeddings not built for {diff}.", file=sys.stderr)

    print(f"{len(special_emb)} {len(list(special_emb.values())[0])}", file=args.output_file)
    for key, value in special_emb.items():
        print(f"{key} {' '.join(value)}", file=args.output_file)


if __name__ == '__main__':
    main()