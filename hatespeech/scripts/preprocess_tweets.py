import re
import sys
import json
import argparse

import emoji
import pandas as pd
from tqdm import tqdm
from spacy.util import get_lang_class
from spacy.attrs import ORTH # pylint: disable=no-name-in-module
from ekphrasis.classes.preprocessor import TextPreProcessor


def get_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser()

    parser.add_argument('input_file', type=argparse.FileType(),
                        help="A data file to process")

    parser.add_argument('--delimiter', type=str, default=',',
                        help="A delimiter which separates fields in data file.")

    parser.add_argument('--text_field', type=str, default='tweet',
                        help="Name or number of the field with tweet texts.")

    parser.add_argument('--label_field', type=str, default='class',
                        help="Name or number of the field with labels.")

    parser.add_argument('--label_mapping', type=str, default='{"0":0, "1":1}',
                        help="Mapping between the data classes and output file classes.")

    parser.add_argument('--output_file', type=argparse.FileType(mode='w'), default=sys.stdout,
                        help="Name of the output file.")

    parser.add_argument('--language', default='en',
                        help="Language, used by tokenizer.")

    return parser.parse_args()


def get_preprocessor():
    """Builds ekphrasis preprocessor."""
    return TextPreProcessor(
        normalize=['email', 'percent', 'money', 'phone',
                   'user', 'time', 'url', 'date'],
        fix_html=True,
        unpack_hashtags=True,
        unpack_contractions=True,
        segmenter="twitter"
    )


def get_tokenizer(lang):
    """Build spacy tokenizer. Add rules for special tokens."""
    tokenizer = get_lang_class(lang).Defaults.create_tokenizer()

    tokenizer.add_special_case("<email>", [{ORTH: "<email>"}])
    tokenizer.add_special_case("<percent>", [{ORTH: "<percent>"}])
    tokenizer.add_special_case("<money>", [{ORTH: "<money>"}])
    tokenizer.add_special_case("<phone>", [{ORTH: "<phone>"}])
    tokenizer.add_special_case("<user>", [{ORTH: "<user>"}])
    tokenizer.add_special_case("<time>", [{ORTH: "<time>"}])
    tokenizer.add_special_case("<url>", [{ORTH: "<url>"}])
    tokenizer.add_special_case("<date>", [{ORTH: "<date>"}])
    
    return tokenizer


def process_tweet(preprocessor, tokenizer, tweet):
    """Cleans and tokenizes tweet text."""
    # Replace html decimal emoji codes with unicode
    tweet = re.sub(r'(&#\d+;)+', lambda x: ' ' + ''.join([chr(int(num[2:])) for num in x.group().split(';') if num]) + ' ', tweet)
    
    # Preprocess text with ekphrasis:
    # - normalize users, urls, dates etc.
    # - unpack hashtags, if the language is en it is based on dictionary lookup
    #   otherwise it is only split on camel case.
    # - unpack contractions: can't -> can not
    tweet = preprocessor.pre_process_doc(tweet)
    
    # Catch rest of the urls
    tweet = re.sub(r'http[s]?://?[\w.\d…]*', '<url>', tweet)

    # Remove punctuation and other symbols (RT, &amp)
    tweet = re.sub(r'RT|&amp|\\n|[|:@,;&!()[\]?.…"”’“‘\'*=_]', ' ', tweet)
    
    # Split text into tokens.
    tweet = [str(token).lower() for token in tokenizer(tweet) if not str(token).isspace()]

    # Replace numbers with special tokens, must be done here after tokenization
    # to keep the emojis intact
    tweet = ["<number>" if token.isnumeric() else token for token in tweet]

    return tweet


def split_tokens_emoji(words):
    """Splits the list of words into text tokens and emojis."""
    emojis, tokens = [], []

    for word in words:
        
        # Handle the tokenizer error when the emoji
        # is inside the word.
        subwords = []
        prefix = 0
        for i, c in enumerate(word):
            if c in emoji.UNICODE_EMOJI:
                emojis.append(c)
                subwords.append(word[prefix:i])
                prefix = i + 1 
        subwords.append(word[prefix:len(word)])

        for subword in subwords:
            # Match the extended unicode emojis
            if re.match(u'[\U0001F1E6-\U0001F1FF]', subword):
                emojis.append(subword)
            elif subword:
                tokens.append(subword)

    return tokens, emojis


def main():
    args = get_args()

    if args.text_field.isdigit():
        args.text_field = int(args.text_field)

    if args.label_field.isdigit():
        args.label_field = int(args.label_field)

    preprocessor = get_preprocessor() 
    tokenizer = get_tokenizer(args.language)
    mapping = json.loads(args.label_mapping)

    data = pd.read_csv(args.input_file, delimiter=args.delimiter)

    for _, row in tqdm(data.iterrows(), total=len(data)):
        label = mapping[str(row[args.label_field])]
        tweet = row[args.text_field]
        
        words = process_tweet(preprocessor, tokenizer, tweet)
        tokens, emojis = split_tokens_emoji(words)

        print(json.dumps({
            'tweet': tweet,
            'tokens': tokens,
            'emojis': emojis,
            'label': label
        }), file=args.output_file)


if __name__ == "__main__":
    main()
