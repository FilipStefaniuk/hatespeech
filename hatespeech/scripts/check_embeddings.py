import json
import argparse
import operator
import itertools
from typing import Iterable, List
from collections import Counter

from allennlp.common import Params
from allennlp.modules.token_embedders import TokenEmbedder, Embedding
from allennlp.data.vocabulary import Vocabulary

from hatespeech.data.dataset_readers import TwitterReader


def get_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser()

    parser.add_argument('data', type=argparse.FileType())
    parser.add_argument('vectors', type=argparse.FileType())
    parser.add_argument('--emojis', action="store_true")

    return parser.parse_args()


def get_data_tokens(data: Iterable, label=None, emojis=False) -> Counter:
    """Counts tokens in the dataset."""
    data = map(json.loads, data)
    if label is not None:
        data = filter(lambda x: x["label"] == label, data)
    
    if emojis:
        data = map(operator.itemgetter("emojis"), data)
    else:
        data = map(operator.itemgetter("tokens"), data)
    
    return Counter(itertools.chain.from_iterable(data))


def get_emb_tokens(vectors: Iterable) -> List[str]:
    next(vectors)
    return list(map(lambda x: x.split()[0], vectors))


def print_stats(most_common, sum1, sum2, uniq1, uniq2, title=None):
    print("*" * 41)
    if title:
        print(f"{title.capitalize():^41}")
    print("*" * 41)
    print("-" * 41)
    print("     MOST COMMON WORDS WITHOUT EMB      ")
    print("-" * 41)
    for w, c in most_common:
        print(f"{repr(w):37}{c:>4}")
    print("-" * 41)

    if sum1 != 0:
        print(f"OOV COUNT: {sum2} / {sum1} ({(sum2 / sum1) * 100:.2f} %)")
    else:
        print(f"OOV COUNT: -")

    if uniq1 != 0:
        print(f"OOV: {uniq2} / {uniq1} ({(uniq2 / uniq1) * 100:.2f} %)")
    else:
        print(f"OOV: -")


def compute_stats(counter: Counter, emb_tokens: List[str], title=""):
    sum_tokens = sum(counter.values())
    uniq_tokens = len(counter)

    # Remove items that have embeddings
    for token in emb_tokens:
        if token in counter:
            del counter[token]

    del counter["<user>"]
    del counter["<number>"]
    del counter["<url>"]
    del counter["<money>"]
    del counter["<percent>"]
    del counter["<date>"]
    del counter["<time>"]

    sum_tokens2 = sum(counter.values())
    uniq_tokens2 = len(counter)

    print_stats(counter.most_common(10), sum_tokens,
                  sum_tokens2, uniq_tokens, uniq_tokens2,
                  title=title)

def main():
    args = get_args()
    data = list(args.data)
    emb_tokens = get_emb_tokens(args.vectors)
    compute_stats(get_data_tokens(data, emojis=args.emojis), emb_tokens,
        title="all")


if __name__ == '__main__':
    main()