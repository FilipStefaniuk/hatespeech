import tweepy
import os
import pickle
import argparse
import logging
import pandas as pd
import numpy as np
from tqdm import tqdm


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('data', type=argparse.FileType(), help="data file with twitter ids.")
    parser.add_argument('consumer_key', help="twitter API consumer key.")
    parser.add_argument('consumer_secret', help="twitter API consumer secret.")
    parser.add_argument('--cache_dict', default="tweets.pkl", help="dictionary with cached tweets.")
    parser.add_argument('--ids_column', default=0, help="name or number of the column with tweet ids.")
    parser.add_argument('--batch_size', default=100, help="batch size of tweet ids")
    parser.add_argument('--output_file', type=argparse.FileType(mode='w'), default=None, help="output file")

    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()
    logging.getLogger().setLevel(logging.INFO)

    cache = {}
    if os.path.exists(args.cache_dict):
        with open(args.cache_dict, mode='rb') as cache_f:
            cache = pickle.load(cache_f)
    
    else:
        logging.info("No cache dictionary found - creating new one.")

    data = pd.read_csv(args.data)
    twitter_ids = data[data.columns[args.ids_column]] if type(args.ids_column) == int else data[args.ids_column]
    
    twitter_texts = [None] * len(twitter_ids)
    batch = []

    auth = tweepy.OAuthHandler(args.consumer_key, args.consumer_secret)
    api = tweepy.API(auth)

    try:
        for i, twitter_id in tqdm(enumerate(twitter_ids), total=len(twitter_ids)):

            if twitter_id in cache:
                twitter_texts[i] = cache[twitter_id]
            else:
                batch.append((i, twitter_id))

            if len(batch) >= args.batch_size or (i + 1 == len(twitter_ids) and len(batch) > 0):
                batch_idxs, batch_ids = zip(*batch)
                map_ = {tweet.id: tweet.text for tweet in api.statuses_lookup(batch_ids)}
                
                for batch_idx, batch_id in batch:
                    twitter_texts[batch_idx] = map_.get(batch_id, '')
                    cache[batch_id] = twitter_texts[batch_idx]

                batch = []


    except tweepy.TweepError:
        logging.error("Exceeded rate limit or invalid api keys, saving cache in %s", args.cache_dict)
    
    with open(args.cache_dict, mode='wb') as cache_f:
        pickle.dump(cache, cache_f)

    data['twitter_text'] = pd.Series(twitter_texts).replace('', np.nan)
    num_instances = len(data)
    
    data.dropna(subset=['twitter_text'], inplace=True)
    num_cleaned_instances = len(data)

    logging.info("Found %d/%d twitter texts", num_cleaned_instances, num_instances)

    if args.output_file:
        logging.info("Saving csv to %s", args.output_file)
        data.to_csv(args.output_file)