from typing import Dict, Optional

from overrides import overrides
import torch

from allennlp.data import Vocabulary
from allennlp.models.model import Model
from allennlp.modules import Seq2SeqEncoder, Seq2VecEncoder, TextFieldEmbedder
from allennlp.nn import InitializerApplicator, RegularizerApplicator
from allennlp.nn.util import get_text_field_mask
from allennlp.training.metrics import CategoricalAccuracy, FBetaMeasure


@Model.register("twitter_classifier")
class TwitterClassifier(Model):
    def __init__(self,
                 vocab: Vocabulary,
                 token_embedder: TextFieldEmbedder,
                 seq2vec_encoder: Seq2VecEncoder,
                 seq2seq_encoder: Optional[Seq2SeqEncoder] = None,
                 emoji_embedder: Optional[TextFieldEmbedder] = None,
                 emoji_encoder: Optional[Seq2VecEncoder] = None,
                 dropout: Optional[float] = None,
                 num_labels: Optional[int] = None,
                 label_namespace: str = "labels",
                 initializer: InitializerApplicator = InitializerApplicator(),
                 regularizer: Optional[RegularizerApplicator] = None):
        
        super().__init__(vocab, regularizer)
        self._token_embedder = token_embedder
        self._emoji_embedder = emoji_embedder
        self._seq2seq_encoder = seq2seq_encoder
        self._seq2vec_encoder = seq2vec_encoder
        self._emoji_encoder = emoji_encoder
        self._classifier_input_dim = self._seq2vec_encoder.get_output_dim()
        
        if self._emoji_embedder and self._emoji_encoder:
            self._classifier_input_dim += self._emoji_encoder.get_output_dim()
        
        self._dropout = torch.nn.Dropout(dropout) if dropout else None
        self._label_namespace = label_namespace
        self._num_labels = num_labels or vocab.get_vocab_size(namespace=self._label_namespace)
        self._classification_layer = torch.nn.Linear(self._classifier_input_dim, self._num_labels)
        self._accuracy = CategoricalAccuracy()
        self._f1 = FBetaMeasure(average="macro")
        self._loss = torch.nn.CrossEntropyLoss()
        initializer(self)

    @overrides
    def forward(self,
                tweet: str,
                tokens: Dict[str, torch.tensor],
                emojis: Dict[str, torch.tensor],
                label: Optional[torch.tensor] = None) -> Dict[str, torch.Tensor]:
        embedded_text = self._token_embedder(tokens)
        mask = get_text_field_mask(tokens).float()

        if self._seq2seq_encoder:
            embedded_text = self._seq2seq_encoder(embedded_text, mask=mask)
        
        embedded_text = self._seq2vec_encoder(embedded_text, mask=mask)

        features = [embedded_text]

        if self._emoji_embedder and self._emoji_encoder:
            emojis_mask = get_text_field_mask(emojis)
            embedded_emojis = self._emoji_embedder(emojis)
            embedded_emojis = self._emoji_encoder(embedded_emojis, mask=emojis_mask)
            features.append(embedded_emojis)

        features = torch.cat(features, dim=-1)  # pylint: disable=no-member

        if self._dropout:
            features = self._dropout(features)

        logits = self._classification_layer(features)
        probs = torch.nn.functional.softmax(logits, dim=-1)

        output_dict = {
            "logits": logits,
            "probs": probs
        }

        if label is not None:
            loss = self._loss(logits, label.long().view(-1))
            output_dict["loss"] = loss
            self._accuracy(logits, label)
            self._f1(logits, label)

        return output_dict

    @overrides
    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        return {
            'accuracy': self._accuracy.get_metric(reset),
            'f1_macro': self._f1.get_metric(reset)["fscore"]
        }


    @overrides
    def decode(self, output_dict: Dict[str, torch.Tensor]) -> Dict[str, torch.Tensor]:
        index = torch.argmax(output_dict['probs']).item()  # pylint: disable=no-member
        label = self.vocab.get_token_from_index(index, namespace=self._label_namespace)

        output_dict['label'] = label
        return output_dict