from typing import Dict, Optional

import torch
from numpy.random import default_rng
from overrides import overrides
from allennlp.models import Model
from allennlp.training.metrics import CategoricalAccuracy, FBetaMeasure

@Model.register("random_classifier")
class RandomClassifier(Model):
    def __init__(self,
                vocab,
                seed = 321,
                label_namespace: str = "labels",):
        super().__init__(vocab)
        self._rng = default_rng(seed)
        self._label_namespace = label_namespace
        self._label_namespace = vocab.get_vocab_size(namespace=self._label_namespace)
        self._accuracy = CategoricalAccuracy()
        self._f1 = FBetaMeasure(average="macro")

    def forward(self,
                tweet: str,
                tokens: Dict[str, torch.tensor],
                emojis: Dict[str, torch.tensor],
                label: Optional[torch.tensor] = None) -> Dict[str, torch.Tensor]:    

        logits = self._rng.standard_normal((tokens['tokens'].shape[0], self._label_namespace))
        logits = torch.from_numpy(logits).float()
        probs = torch.nn.functional.softmax(logits, dim=-1)
        
        output_dict = {
            "logits": logits,
            "probs": probs
        }

        if label is not None:
            self._accuracy(logits, label)
            self._f1(logits, label)

        return output_dict


    @overrides
    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        return {
            'accuracy': self._accuracy.get_metric(reset),
            'f1_macro': self._f1.get_metric(reset)["fscore"]
        }

    @overrides
    def decode(self, output_dict: Dict[str, torch.Tensor]) -> Dict[str, torch.Tensor]:
        index = torch.argmax(output_dict['probs']).item()  # pylint: disable=no-member
        label = self.vocab.get_token_from_index(index, namespace=self._label_namespace)

        output_dict['label'] = label
        return output_dict