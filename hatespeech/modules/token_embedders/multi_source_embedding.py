import tempfile
from typing import List

import torch
from allennlp.modules.token_embedders import Embedding, TokenEmbedder
from allennlp.modules.token_embedders.embedding import EmbeddingsTextFile, _read_pretrained_embeddings_file
from allennlp.data.vocabulary import Vocabulary
from allennlp.common import Params

@TokenEmbedder.register("multi_source_embedding")
class MultiSourceEmbedding(Embedding):
    """Embedding that allows loading word embeddings from multiple files."""

    def __init__(self,
                 embedding_dim: int,
                 num_embeddings: int = None,
                 projection_dim: int = None,
                 weight: torch.FloatTensor = None,
                 padding_index: int = None,
                 trainable: bool = True,
                 max_norm: float = None,
                 norm_type: float = 2.0,
                 scale_grad_by_freq: bool = False,
                 sparse: bool = False,
                 vocab_namespace: str = "tokens",
                 pretrained_files: List[str] = None):
        super().__init__(embedding_dim=embedding_dim, num_embeddings=num_embeddings,
                         projection_dim=projection_dim, weight=weight, padding_index=padding_index,
                         trainable=trainable, max_norm=max_norm, norm_type=norm_type, 
                         scale_grad_by_freq=scale_grad_by_freq, sparse=sparse, vocab_namespace=vocab_namespace,
                         pretrained_file=None)
        self._pretrained_files = pretrained_files

    
    @classmethod
    def from_params(cls, vocab: Vocabulary, params: Params) -> 'Embedding':  # type: ignore
        num_embeddings = params.pop_int('num_embeddings', None)
        vocab_namespace = params.pop("vocab_namespace", None if num_embeddings else "tokens")
        num_embeddings = num_embeddings or vocab.get_vocab_size(vocab_namespace)
        embedding_dim = params.pop_int('embedding_dim')
        pretrained_files = params.pop("pretrained_files", None)
        projection_dim = params.pop_int("projection_dim", None)
        padding_index=params.pop_int('padding_index', None)
        trainable=params.pop_bool("trainable", True)
        max_norm=params.pop_float('max_norm', None)
        norm_type=params.pop_float('norm_type', 2.)
        scale_grad_by_freq=params.pop_bool('scale_grad_by_freq', False)
        sparse=params.pop_bool('sparse', False)
        params.assert_empty(cls.__name__)

        if pretrained_files:

            # Create temporary file that merges the multiple sources
            # Currently does not work for hdf5 files
            pretrained_file = tempfile.NamedTemporaryFile(mode="w", suffix=".vec")
            for file_uri in pretrained_files:
                with EmbeddingsTextFile(file_uri) as embedding_file:
                    for line in embedding_file:
                        pretrained_file.write(line)


            # If we're loading a saved model, we don't want to actually read a pre-trained
            # embedding file - the embeddings will just be in our saved weights, and we might not
            # have the original embedding file anymore, anyway.
            weight = _read_pretrained_embeddings_file(pretrained_file.name,
                                                      embedding_dim,
                                                      vocab,
                                                      vocab_namespace)
            
            pretrained_file.close()
        else:
            weight = None

        return cls(num_embeddings=num_embeddings,
                   embedding_dim=embedding_dim,
                   projection_dim=projection_dim,
                   weight=weight,
                   padding_index=padding_index,
                   trainable=trainable,
                   max_norm=max_norm,
                   norm_type=norm_type,
                   scale_grad_by_freq=scale_grad_by_freq,
                   sparse=sparse,
                   vocab_namespace=vocab_namespace)