#!/bin/bash

RAW_DIR='data/raw/'
PROCESSED_DIR='data/processed/'

# Output directories
AUG_DIR="${PROCESSED_DIR}other/"
PRETRAIN_DIR="${PROCESSED_DIR}pretrain/"

# Preprocess english data for augmentation
# maps class values to 0 - non-hate and 1 - hate

# -----------------------------------------------------------------------------
#   PREPROCESS THE DATASET FOR PRETRAINING
# -----------------------------------------------------------------------------

# originally: 0 - hate speech, 1 - offensive language, 2 - neither
preprocess_tweets "${RAW_DIR}/dt.csv" --label_mapping '{"0": 1, "1": 0, "2": 0}' --output_file "${AUG_DIR}/dt.jsonl"

# originally: spam, abusive, normal, hateful 
preprocess_tweets "${RAW_DIR}/lsc.csv" --delimiter '\t' --label_field 1 --text_field 0 \
 --label_mapping '{"spam": 0, "abusive": 0, "normal": 0, "hateful": 1}' --output_file "${AUG_DIR}/lsc.jsonl"

# originally: H - hateful, N - nonhateful
preprocess_tweets "${RAW_DIR}/oh.csv" --label_field "Code" --text_field "Tweet" \
--label_mapping '{"H": 1, "N": 0}' --output_file "${AUG_DIR}/oh.jsonl"

# originally: 0 - racist, 1 - sexsist, 2 - non-offensive, 3 - racist and sexist
preprocess_tweets "${RAW_DIR}/wz.csv" --label_field "class" --text_field "twitter_text"\
 --label_mapping '{"0": 1, "1": 1, "2": 0, "3": 1}' --output_file "${AUG_DIR}/wz.jsonl"

build_dataset "${AUG_DIR}/dt.jsonl" "${AUG_DIR}/lsc.jsonl" "${AUG_DIR}/oh.jsonl" "${AUG_DIR}/wz.jsonl" --resample --output_dir "${PRETRAIN_DIR}" --valid 0.1

# -------------------------------------------------------------------------------
# PREPROCESS LANGUAGE DATASETS
# -------------------------------------------------------------------------------

preprocess_tweets "${RAW_DIR}/haspeede/test.tsv" --delimiter '\t' --text_field 1 --label_field 2 --output_file "${PROCESSED_DIR}/ita/test.jsonl" --language it
preprocess_tweets "${RAW_DIR}/haspeede/dev.tsv" --delimiter '\t' --text_field 1 --label_field 2 --output_file "${PROCESSED_DIR}/ita/dev.jsonl" --language it
preprocess_tweets "${RAW_DIR}/haspeede/train.tsv" --delimiter '\t' --text_field 1 --label_field 2 --output_file "${PROCESSED_DIR}/ita/train.jsonl" --language it

preprocess_tweets "${RAW_DIR}/hateval-en/test.tsv" --delimiter '\t' --text_field 1 --label_field 2 --output_file "${PROCESSED_DIR}/eng/test.jsonl"
preprocess_tweets "${RAW_DIR}/hateval-en/dev.tsv" --delimiter '\t' --text_field 1 --label_field 2 --output_file "${PROCESSED_DIR}/eng/dev.jsonl"
preprocess_tweets "${RAW_DIR}/hateval-en/train.tsv" --delimiter '\t' --text_field 1 --label_field 2 --output_file "${PROCESSED_DIR}/eng/train.jsonl"

preprocess_tweets "${RAW_DIR}/hateval-es/test.tsv" --delimiter '\t' --text_field 1 --label_field 2 --output_file "${PROCESSED_DIR}/esp/test.jsonl" --language es
preprocess_tweets "${RAW_DIR}/hateval-es/dev.tsv" --delimiter '\t' --text_field 1 --label_field 2 --output_file "${PROCESSED_DIR}/esp/dev.jsonl" --language es
preprocess_tweets "${RAW_DIR}/hateval-es/train.tsv" --delimiter '\t' --text_field 1 --label_field 2 --output_file "${PROCESSED_DIR}/esp/train.jsonl" --language es

preprocess_tweets "${RAW_DIR}/poleval/test.tsv" --delimiter '\t' --text_field 0 --label_field 1 --output_file "${PROCESSED_DIR}/pol/test.jsonl" --language pl
preprocess_tweets "${RAW_DIR}/poleval/dev.tsv" --delimiter '\t' --text_field 0 --label_field 1 --output_file "${PROCESSED_DIR}/pol/dev.jsonl" --language pl
preprocess_tweets "${RAW_DIR}/poleval/train.tsv" --delimiter '\t' --text_field 0 --label_field 1 --output_file "${PROCESSED_DIR}/pol/train.jsonl" --language pl

preprocess_tweets "${RAW_DIR}/lhsab.csv" --delimiter '\t' --text_field Tweet --label_field Class\
 --label_mapping '{"abusive": 0, "normal": 0, "hate": 1}' --output_file "${PROCESSED_DIR}/other/lhsab.jsonl" --language ar
build_dataset "${AUG_DIR}/lhsab.jsonl" --output_dir "${PROCESSED_DIR}/ara/" --valid 0.1 --test 0.1

preprocess_tweets "${RAW_DIR}/po.csv" --text_field text --label_field hatespeech_comb --output_file "${PROCESSED_DIR}/other/po.jsonl" --language pt
build_dataset "${AUG_DIR}/po.jsonl" --output_dir "${PROCESSED_DIR}/por/" --valid 0.1 --test 0.1