#!/bin/bash

wget -nc -P ./vectors/ https://raw.githubusercontent.com/MirunaPislar/emoji2vec/master/models/emoji_embeddings_300d.txt
wget -nc -P ./vectors/ https://raw.githubusercontent.com/MirunaPislar/emoji2vec/master/models/emoji_embeddings_100d.txt
wget -nc -P ./vectors/ https://dl.fbaipublicfiles.com/arrival/vectors/wiki.multi.ar.vec
wget -nc -P ./vectors/ https://dl.fbaipublicfiles.com/arrival/vectors/wiki.multi.en.vec
wget -nc -P ./vectors/ https://dl.fbaipublicfiles.com/arrival/vectors/wiki.multi.es.vec
wget -nc -P ./vectors/ https://dl.fbaipublicfiles.com/arrival/vectors/wiki.multi.it.vec
wget -nc -P ./vectors/ https://dl.fbaipublicfiles.com/arrival/vectors/wiki.multi.pl.vec
wget -nc -P ./vectors/ https://dl.fbaipublicfiles.com/arrival/vectors/wiki.multi.pt.vec