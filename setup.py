from setuptools import setup, find_packages

setup(
    name="hatespeech",
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'preprocess_tweets=hatespeech.scripts.preprocess_tweets:main',
            'build_dataset=hatespeech.scripts.build_dataset:main',
            'count_tweets=hatespeech.scripts.count_tweets:main',
            'check_embeddings=hatespeech.scripts.check_embeddings:main',
            'build_special_emb=hatespeech.scripts.build_special_emb:main',
            'compute_stats=hatespeech.scripts.compute_stats:main'
        ],
    },
    version="0.0.1",
    install_requires=[
        "allennlp==0.9.0",
        "spacy==2.1.9",
        "ekphrasis==0.5.1",
        "emoji==0.5.4",
        "tqdm==4.45.0",
        "pandas==1.0.3",
        "scikit-learn==0.22.2.post1",
        "tweepy==3.8.0"
    ]
)
