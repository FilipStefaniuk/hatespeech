local train_data = "./data/processed/eng/train.jsonl";
local dev_data = "./data/processed/eng/dev.jsonl";
local batch_size = 32;

{
    dataset_reader: {
        type: "twitter",
    },
    train_data_path: train_data,
    validation_data_path: dev_data,
    model: { type: "random_classifier" },
    iterator: {
        type: "bucket",
        batch_size: batch_size,
        sorting_keys: [["tokens", "num_tokens"]],
    },
    trainer: {
        type: "no_op",
    },
}