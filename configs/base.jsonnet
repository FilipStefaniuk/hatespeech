local train_data = "./data/processed/eng/train.jsonl";
local dev_data = "./data/processed/eng/dev.jsonl";
local embeddings = ['./vectors/wiki.multi.en.vec', './vectors/special.multi.vec'];
local embedding_dim = 300;
local batch_size = 32;
local num_epochs = 20;
local learning_rate = 0.001;
local patience = 5;

{
    dataset_reader: {
        type: "twitter",
        min_num_tokens: 5
    },
    train_data_path: train_data,
    validation_data_path: dev_data,
    model: {
        type: "twitter_classifier",
        token_embedder: {
            token_embedders: {
                "tokens": {
                    type: "multi_source_embedding",
                    embedding_dim: embedding_dim,
                    trainable: false,
                    pretrained_files: embeddings
                },
            },
        },
        seq2vec_encoder: {
            type: "cnn",
            embedding_dim: embedding_dim,
            num_filters: 300
        },
    },
    iterator: {
        type: "bucket",
        batch_size: batch_size,
        sorting_keys: [["tokens", "num_tokens"]],
    },
    trainer: {
        num_epochs: num_epochs,
        optimizer: {
            type: "adam",
            lr: learning_rate
        },
        patience: patience
    },
}